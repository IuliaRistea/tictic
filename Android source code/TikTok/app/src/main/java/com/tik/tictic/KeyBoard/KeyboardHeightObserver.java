package com.kyadav.tictic.KeyBoard;


public interface KeyboardHeightObserver {

    void onKeyboardHeightChanged(int height, int orientation);
}
